#include<stdio.h>

//STAGE_X>=HOHEI_X*2,STAGE_Y>=HOHEI_Y*2
#define STAGE_X 20
#define STAGE_Y 20
#define HOHEI_X 6
#define HOHEI_Y 6

struct hohei{int hp; int x; int y; int moved;};
struct around{int x; int y; int exist;};

void initialize(struct hohei* a, struct hohei* b);
void print_stage(struct hohei* a, struct hohei* b);
void play_game(struct hohei* a, struct hohei* b);
int check_hohei(struct hohei* hoheis);
int select(struct hohei* hoheis, char team_name);
void move(struct hohei* hohei);
int attack(struct hohei own, struct hohei* enemy, int team);
int check_finish(struct hohei* a, struct hohei* b);

int stage[STAGE_X][STAGE_Y] = {};

int main()
{ 
  struct hohei a[HOHEI_X*HOHEI_Y], b[HOHEI_X*HOHEI_Y];
  initialize(a, b);
  play_game(a, b);
  return 0;
}

void initialize(struct hohei* a, struct hohei* b)
{
  int i=0,j=0,x=0,y=0;
  for(x=0;x<STAGE_X;x++){
    for(y=0;y<STAGE_Y;y++){
      if(x>=STAGE_X-HOHEI_X && y<=HOHEI_Y-1){
        stage[x][y] = 1;
        a[i].hp = 2;  a[i].x = x;  a[i].y = y; a[i].moved = 0; i++;
      }
      if(x<=HOHEI_X-1 && y>=STAGE_Y-HOHEI_Y){
        stage[x][y] = 2;
        b[j].hp = 2;  b[j].x = x;  b[j].y = y; b[j].moved = 0; j++;
      }
    }
  }
}

void print_stage(struct hohei* a, struct hohei* b)
{
  int x,y,i;
  printf("   ");
  for(x=0;x<STAGE_X;x++) printf("%4d ",x);
  printf("\n");
  for(y=0;y<STAGE_Y;y++){
    printf("%2d ",y);
    for(x=0;x<STAGE_X;x++){
      if(stage[x][y]==1){
        for(i=0;i<HOHEI_X*HOHEI_Y;i++){
          if(a[i].x==x && a[i].y==y && a[i].hp!=0){
            if(a[i].moved==0) printf("%2da%d ",i,a[i].hp);
            else              printf("%2dc%d ",i,a[i].hp);
          }
        }
      }else if(stage[x][y]==2){
        for(i=0;i<HOHEI_X*HOHEI_Y;i++){
          if(b[i].x==x && b[i].y==y && b[i].hp!=0){
            if(b[i].moved==0) printf("%2db%d ",i,b[i].hp);
            else              printf("%2dd%d ",i,b[i].hp);
          }
        }
      }else{
        printf(" --- ");
      }
    }
    printf("\n");
  }
}

void play_game(struct hohei* a, struct hohei* b)
{
  int check_f=0, num_a, num_b, i, hohei, att;
  while(check_f==0){
    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      a[i].moved=0; b[i].moved=0;
    }
    print_stage(a, b);
    num_a = check_hohei(a);
    for(i=0;i<num_a;i++){
      hohei = select(a, 'a');
      move(&a[hohei]);
      print_stage(a, b);
      att = attack(a[hohei], b, 1);
      if(att==1) print_stage(a, b);
      check_f = check_finish(a, b);
      if(check_f!=0) break;
    }
    num_b = check_hohei(b);
    for(i=0;i<num_b;i++){
      hohei = select(b, 'b');
      move(&b[hohei]);
      print_stage(a, b);
      att = attack(b[hohei], a, 2);
      if(att==1) print_stage(a, b);
      check_f = check_finish(a, b);
      if(check_f!=0) break;
    }
  }
  if(check_f==1) printf("team_a win!\n");
  else printf("team_b win!\n");
}

int check_hohei(struct hohei* hoheis)
{
  int i, num=0;
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(hoheis[i].hp!=0) num++;
  }
  return num;
}

int select(struct hohei* hoheis, char team_name)
{
  int i, check=0;
  while(check!=1){
    printf("select team_%c hohei's number: ", team_name);
    scanf("%d", &i);
    if(hoheis[i].hp==0 || i>HOHEI_X*HOHEI_Y || hoheis[i].moved==1)
      printf("can't select!\n");
    else check=1;
  }
  return i;
}

void move(struct hohei* hohei)
{
  int move, check=0, x, y, team=stage[hohei->x][hohei->y], enemy;
  char dummy;
  if(team==1) enemy=2;
  else        enemy=1;
  while(check!=1){
    printf("move?(yes=1/no=0): ");
    scanf("%d", &move);
    scanf("%c", &dummy);
    if(move==0){
      hohei->moved=1; break;
    }
    printf("where?\n");
    printf("x="); scanf("%d",&x); scanf("%c", &dummy);
    printf("y="); scanf("%d",&y); scanf("%c", &dummy);
    if(x>=STAGE_X || y>=STAGE_Y || stage[x][y]!=0 || abs(x-hohei->x)+abs(y-hohei->y)>3){
      printf("can't move!\n"); continue;
    }
    if(y==hohei->y && x-hohei->x<0 && stage[hohei->x-1][hohei->y]==enemy && (x==hohei->x-2 || x==hohei->x-3)){
      printf("can't move!\n"); continue;
    }
    if(y==hohei->y && x-hohei->x<0 && stage[hohei->x-2][hohei->y]==enemy && x==hohei->x-3){
      printf("can't move!\n"); continue;
    }
    if(y==hohei->y && x-hohei->x>0 && stage[hohei->x+1][hohei->y]==enemy && (x==hohei->x+2 || x==hohei->x+3)){
      printf("can't move!\n"); continue;
    }
    if(y==hohei->y && x-hohei->x>0 && stage[hohei->x+2][hohei->y]==enemy && x==hohei->x+3){
      printf("can't move!\n"); continue;
    }
    if(x==hohei->x && y-hohei->y<0 && stage[hohei->x][hohei->y-1]==enemy && (y==hohei->y-2 || y==hohei->y-3)){
      printf("can't move!\n"); continue;
    }
    if(x==hohei->x && y-hohei->y<0 && stage[hohei->x][hohei->y-2]==enemy && y==hohei->y-3){
      printf("can't move!\n"); continue;
    }
    if(x==hohei->x && y-hohei->y>0 && stage[hohei->x][hohei->y+1]==enemy && (y==hohei->y+2 || y==hohei->y+3)){
      printf("can't move!\n"); continue;
    }
    if(x==hohei->x && y-hohei->y>0 && stage[hohei->x][hohei->y+2]==enemy && y==hohei->y+3){
      printf("can't move!\n"); continue;
    }
    stage[hohei->x][hohei->y]=0;
    stage[x][y]=team;
    hohei->x=x;
    hohei->y=y;
    hohei->moved=1;
    check=1;
  }
}

int abs(int num)
{
  if(num < 0) return -num;
  else return num;
}

int attack(struct hohei own, struct hohei* enemy, int team)
{
  struct around e[4];
  int i,j,attack,team_enemy,num_enemy=0,check=0,num;
  char dummy;

  if(team==1) team_enemy=2;
  else        team_enemy=1;

  if(own.x-1<0){
    e[0].x=0; e[0].y=0; e[0].exist=0;
  }else{
    e[0].x=own.x-1; e[0].y=own.y; e[0].exist=stage[e[0].x][e[0].y];
  }
  if(own.x+1>STAGE_X-1){
    e[1].x=0; e[1].y=0; e[1].exist=0;
  }else{
    e[1].x=own.x+1; e[1].y=own.y; e[1].exist=stage[e[1].x][e[1].y];
  }
  if(own.y-1<0){
    e[2].x=0; e[2].y=0; e[2].exist=0;
  }else{
    e[2].x=own.x; e[2].y=own.y-1; e[2].exist=stage[e[2].x][e[2].y];
  }
  if(own.y+1>STAGE_Y-1){
    e[3].x=0; e[3].y=0; e[3].exist=0;
  }else{
    e[3].x=own.x; e[3].y=own.y+1; e[3].exist=stage[e[3].x][e[3].y];
  }

  for(i=0;i<4;i++){
    if(e[i].exist==team_enemy) num_enemy++;
  }

  if(num_enemy!=0){
    while(check!=1){
      printf("attack?(yes=1/no=0): ");
      scanf("%d",&attack);
      scanf("%c",&dummy);
      if(attack==0) break;
      if(num_enemy==1){
	for(i=0;i<4;i++){
	  if(e[i].exist==team_enemy) break;
	}
	for(j=0;j<HOHEI_X*HOHEI_Y;j++){
	  if(enemy[j].x==e[i].x && enemy[j].y==e[i].y){
	    enemy[j].hp--;
	    if(enemy[j].hp==0)
	      stage[enemy[j].x][enemy[j].y]=0;
	    break;
          }
	}
        check=1;
      }else{
        printf("who?: "); scanf("%d",&num);
        if(num>=HOHEI_X*HOHEI_Y) printf("can't attack!\n");
        for(i=0;i<4;i++){
          if(e[i].x==enemy[num].x && e[i].y==enemy[num].y && e[i].exist==team_enemy){
            enemy[num].hp--;
            if(enemy[num].hp==0)
              stage[enemy[num].x][enemy[num].y]=0;
            check=1; break;
          }
        }
        if(check!=1) printf("can't attack!\n");
      }
    }
  }
  if(num_enemy==0) return num_enemy;
  else return attack;
}

int check_finish(struct hohei* a, struct hohei* b)
{
  int i,check_a=0,check_b=0;
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(a[i].hp!=0){
      check_a=1; break;
    }
  }
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(b[i].hp!=0){
      check_b=1; break;
    }
  }
  if(check_a==1 && check_b==1) return 0;
  else if(check_b==0) return 1;
  else if(check_a==0) return 2;
}
