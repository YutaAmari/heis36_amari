//AI3>AI2>AI4

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//STAGE_X>=HOHEI_X*2,STAGE_Y>=HOHEI_Y*2
#define STAGE_X 20
#define STAGE_Y 20
#define HOHEI_X 6
#define HOHEI_Y 6
#define BATTLE 10000

struct hohei{int hp; int x; int y; int moved;};
struct around{int x; int y; int team;};

void initialize(struct hohei* a, struct hohei* b);
void print_stage(struct hohei* a, struct hohei* b);
int play_game(struct hohei* a, struct hohei* b);
int check_hohei(struct hohei* hoheis);
int selectp(struct hohei* hoheis, char team_name);
int select_AI(struct hohei* hoheis, int num);
void move(struct hohei* hohei);
int move_AI(struct hohei* hohei, int move, int x, int y);
int attack(struct hohei own, struct hohei* enemy, int team);
int around_AI(struct around* e, struct hohei own, int team);
int attack_AI(struct hohei* enemy, int team, struct around* e, int num_enemy, int num, int attack);
int check_finish(struct hohei* a, struct hohei* b);
void AI(struct hohei* own, struct hohei* enemy, int team);  //ランダム移動
void AI2(struct hohei* own, struct hohei* enemy, int team); //ランダム移動 ＋ HP1優先攻撃
void AI3(struct hohei* own, struct hohei* enemy, int team); //敵の少ない場所に移動
void AI4(struct hohei* own, struct hohei* enemy, int team); //敵の少ない場所に移動（敵と接している場合は移動しない）
void AI5(struct hohei* own, struct hohei* enemy, int team); //近くの敵の少ない場所に移動（未完成）
int can_move(int h_x, int h_y, int x, int y, int enemy);
int density(int x, int y, int side_length, int enemy);
int min_dens(int *dens, int area_num);
void heis_input(struct hohei *own, struct hohei *enemy, int team);
int select_h(struct hohei* hoheis, int x, int y);
int move_h(struct hohei* hohei, int x, int y);
int attack_h(struct hohei own, struct hohei* enemy, int team, int x, int y);

int stage[STAGE_X][STAGE_Y] = {};

int main()
{
  int a_win=0, b_win=0, win, match, i=0;
  int t[BATTLE], sum=0;
  double ave, hennsa=0;
  struct hohei a[HOHEI_X*HOHEI_Y], b[HOHEI_X*HOHEI_Y];
  for(match=1;match<=BATTLE;match++){
    initialize(a, b);
    win = play_game(a, b);
    if(win==1)
      //printf("team_a win!\n");
      a_win++;
    else
      //printf("team_b win!\n");
      b_win++;
    //t[i] = play_game(a, b); i++;
  }
  printf("a_win=%d, b_win=%d\n", a_win, b_win);
  /*for(i=0;i<BATTLE;i++)
    sum += t[i];
  ave = (double)sum/BATTLE;
  for(i=0;i<BATTLE;i++)
    hennsa += ((double)t[i]-ave)*((double)t[i]-ave);
  hennsa /= BATTLE;
  printf("ave=%f, hennsa=%f\n", ave, hennsa);
  hennsa = 0;
  for(i=0;i<BATTLE;i++)
    hennsa += (double)t[i]*(double)t[i];
  hennsa = hennsa/BATTLE - ave*ave;
  printf("ave=%f, hennsa=%f\n", ave, hennsa);*/
  return 0;
}

void initialize(struct hohei* a, struct hohei* b)
{
  int i=0,j=0,x=0,y=0;
  for(x=0;x<STAGE_X;x++){
    for(y=0;y<STAGE_Y;y++){
      if(x>=STAGE_X-HOHEI_X && y<=HOHEI_Y-1){
        stage[x][y] = 1;
        a[i].hp = 2;  a[i].x = x;  a[i].y = y; a[i].moved = 0; i++;
      }
      else if(x<=HOHEI_X-1 && y>=STAGE_Y-HOHEI_Y){
        stage[x][y] = 2;
        b[j].hp = 2;  b[j].x = x;  b[j].y = y; b[j].moved = 0; j++;
      }
      else stage[x][y] = 0;
    }
  }
}

void print_stage(struct hohei* a, struct hohei* b)
{
  int x,y,i;
  //system("clear");
  printf("\x1b[49m");
  printf("   ");
  for(x=0;x<STAGE_X;x++) printf("%4d ",x);
  printf("\n");
  for(y=0;y<STAGE_Y;y++){
    printf("\x1b[49m");
    printf("%2d ",y);
    for(x=0;x<STAGE_X;x++){
      if(stage[x][y]==1){
        for(i=0;i<HOHEI_X*HOHEI_Y;i++){
          if(a[i].x==x && a[i].y==y && a[i].hp>0){
            printf("\x1b[41m");
            if(a[i].moved==0) printf("%2da%d ",i,a[i].hp);
            else              printf("%2dA%d ",i,a[i].hp);
          }
        }
      }else if(stage[x][y]==2){
        for(i=0;i<HOHEI_X*HOHEI_Y;i++){
          if(b[i].x==x && b[i].y==y && b[i].hp>0){
            printf("\x1b[44m");
            if(b[i].moved==0) printf("%2db%d ",i,b[i].hp);
            else              printf("%2dB%d ",i,b[i].hp);
          }
        }
      }else{
        printf("\x1b[49m");
        printf(" --- ");
      }
    }
    printf("\x1b[49m");
    printf("\n");
  }
  sleep(1);
}

int play_game(struct hohei* a, struct hohei* b)
{
  int turn=0;
  int check_f=0, num_a, num_b, i, hohei, att;

  //print_stage(a, b);

  while(check_f==0){
    turn++;
    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      a[i].moved=0; b[i].moved=0;
    }

    AI2(a, b, 1);

    print_stage(a, b);
    check_f = check_finish(a, b);
    if(check_f!=0) break;

    AI3(b, a, 2);
    //heis_input(b, a, 2);

    print_stage(a, b);
    check_f = check_finish(a, b);
    if(check_f!=0) break;

    /*num_b = check_hohei(b);
    for(i=0;i<num_b;i++){
      hohei = selectp(b, 'b');
      move(&b[hohei]);
      print_stage(a, b);
      att = attack(b[hohei], a, 2);
      if(att==1) print_stage(a, b);
      check_f = check_finish(a, b);
      if(check_f!=0) break;
    }*/
  }
  //printf("turn:%llu\n", turn);
  //if(check_f==1) return 1;
  //else           return 2;
  return turn;
}

int check_hohei(struct hohei* hoheis)
{
  int i, num=0;
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(hoheis[i].hp>0) num++;
  }
  return num;
}

int selectp(struct hohei* hoheis, char team_name)
{
  int i, check=0;
  while(check!=1){
    printf("select team_%c hohei's number: ", team_name);
    scanf("%d", &i);
    if(hoheis[i].hp<=0 || i>HOHEI_X*HOHEI_Y || hoheis[i].moved==1)
      printf("can't select!\n");
    else check=1;
  }
  return i;
}

int select_AI(struct hohei* hoheis, int num)
{
  if(hoheis[num].hp<=0 || num>HOHEI_X*HOHEI_Y || hoheis[num].moved==1) return 0;
  else return 1;
}

void move(struct hohei* hohei)
{
  int move, check=0, x, y, team=stage[hohei->x][hohei->y], enemy;
  char dummy;
  if(team==1) enemy=2;
  else        enemy=1;
  while(check!=1){
    printf("move?(yes=1/no=0): ");
    scanf("%d", &move);
    scanf("%c", &dummy);
    if(move==0){
      hohei->moved=1; break;
    }
    printf("where?\n");
    printf("x="); scanf("%d",&x); scanf("%c", &dummy);
    printf("y="); scanf("%d",&y); scanf("%c", &dummy);
    if(x>=STAGE_X || y>=STAGE_Y || x<0 || y<0 || stage[x][y]!=0 || abs(x-hohei->x)+abs(y-hohei->y)>3){
      printf("can't move!\n"); continue;
    }
    if(can_move(hohei->x, hohei->y, x-hohei->x, y-hohei->y, enemy)!=1){
      printf("can't move!\n"); continue;
    }
    stage[hohei->x][hohei->y]=0;
    stage[x][y]=team;
    hohei->x=x;
    hohei->y=y;
    hohei->moved=1;
    check=1;
  }
}

int move_AI(struct hohei* hohei, int move, int x, int y)
{
  int team=stage[hohei->x][hohei->y], enemy, check=1;
  if(team==1) enemy=2;
  else        enemy=1;
  if(move==0){
    hohei->moved=1; return 1;
  }
  if(x>=STAGE_X || y>=STAGE_Y || x<0 || y<0 || stage[x][y]!=0 || abs(x-hohei->x)+abs(y-hohei->y)>3){
    return 0;
  }
  if(can_move(hohei->x, hohei->y, x-hohei->x, y-hohei->y, enemy)!=1) check=0;
  if(check!=0){
    stage[hohei->x][hohei->y]=0;
    stage[x][y]=team;
    hohei->x=x;
    hohei->y=y;
    hohei->moved=1;
    return 1;
  }
  return 0;
}

int abs(int num)
{
  if(num < 0) return -num;
  else return num;
}

int attack(struct hohei own, struct hohei* enemy, int team)
{
  struct around e[4];
  int i,j,attack,team_enemy,num_enemy=0,check=0,num;
  char dummy;

  if(team==1) team_enemy=2;
  else        team_enemy=1;

  if(own.x-1<0){
    e[0].x=0; e[0].y=0; e[0].team=0;
  }else{
    e[0].x=own.x-1; e[0].y=own.y; e[0].team=stage[e[0].x][e[0].y];
  }
  if(own.x+1>STAGE_X-1){
    e[1].x=0; e[1].y=0; e[1].team=0;
  }else{
    e[1].x=own.x+1; e[1].y=own.y; e[1].team=stage[e[1].x][e[1].y];
  }
  if(own.y-1<0){
    e[2].x=0; e[2].y=0; e[2].team=0;
  }else{
    e[2].x=own.x; e[2].y=own.y-1; e[2].team=stage[e[2].x][e[2].y];
  }
  if(own.y+1>STAGE_Y-1){
    e[3].x=0; e[3].y=0; e[3].team=0;
  }else{
    e[3].x=own.x; e[3].y=own.y+1; e[3].team=stage[e[3].x][e[3].y];
  }

  for(i=0;i<4;i++){
    if(e[i].team==team_enemy) num_enemy++;
  }

  if(num_enemy!=0){
    while(check!=1){
      printf("attack?(yes=1/no=0): ");
      scanf("%d",&attack);
      scanf("%c",&dummy);
      if(attack==0) break;
      if(num_enemy==1){
	for(i=0;i<4;i++){
	  if(e[i].team==team_enemy) break;
	}
	for(j=0;j<HOHEI_X*HOHEI_Y;j++){
	  if(enemy[j].x==e[i].x && enemy[j].y==e[i].y && enemy[j].hp>0){
	    enemy[j].hp--;
	    if(enemy[j].hp<=0)
	      stage[enemy[j].x][enemy[j].y]=0;
	    break;
          }
	}
        check=1;
      }else{
        printf("who?: "); scanf("%d",&num);
        if(num>=HOHEI_X*HOHEI_Y) printf("can't attack!\n");
        for(i=0;i<4;i++){
          if(e[i].x==enemy[num].x && e[i].y==enemy[num].y && e[i].team==team_enemy && enemy[num].hp>0){
            enemy[num].hp--;
            if(enemy[num].hp<=0)
              stage[enemy[num].x][enemy[num].y]=0;
            check=1; break;
          }
        }
        if(check!=1) printf("can't attack!\n");
      }
    }
  }
  if(num_enemy==0) return num_enemy;
  else return attack;
}

int around_AI(struct around* e, struct hohei own, int team)
{
  int num_enemy=0, team_enemy, i;
  if(team==1) team_enemy=2;
  else        team_enemy=1;

  if(own.x-1<0){
    e[0].x=0; e[0].y=0; e[0].team=0;
  }else{
    e[0].x=own.x-1; e[0].y=own.y; e[0].team=stage[e[0].x][e[0].y];
  }
  if(own.x+1>STAGE_X-1){
    e[1].x=0; e[1].y=0; e[1].team=0;
  }else{
    e[1].x=own.x+1; e[1].y=own.y; e[1].team=stage[e[1].x][e[1].y];
  }
  if(own.y-1<0){
    e[2].x=0; e[2].y=0; e[2].team=0;
  }else{
    e[2].x=own.x; e[2].y=own.y-1; e[2].team=stage[e[2].x][e[2].y];
  }
  if(own.y+1>STAGE_Y-1){
    e[3].x=0; e[3].y=0; e[3].team=0;
  }else{
    e[3].x=own.x; e[3].y=own.y+1; e[3].team=stage[e[3].x][e[3].y];
  }

  for(i=0;i<4;i++){
    if(e[i].team==team_enemy) num_enemy++;
  }
  return num_enemy;
}

int attack_AI(struct hohei* enemy, int team, struct around* e, int num_enemy, int num, int attack)
{
  int i,j,team_enemy,check=0;

  if(team==1) team_enemy=2;
  else        team_enemy=1;

  if(attack==0) return 1;
  if(num_enemy==1){
    for(i=0;i<4;i++){
      if(e[i].team==team_enemy) break;
    }
    for(j=0;j<HOHEI_X*HOHEI_Y;j++){
      if(enemy[j].x==e[i].x && enemy[j].y==e[i].y && enemy[j].hp>0){
	enemy[j].hp--;
	if(enemy[j].hp<=0)
	  stage[enemy[j].x][enemy[j].y]=0;
	break;
      }
    }
    return i+num_enemy;
  }else{
    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      if(e[num].x==enemy[i].x && e[num].y==enemy[i].y && e[num].team==team_enemy && enemy[i].hp>0){
        enemy[i].hp--;
        if(enemy[i].hp<=0)
          stage[enemy[i].x][enemy[i].y]=0;
        return num+num_enemy;
      }
    }
  }
  return 0;
}

int check_finish(struct hohei* a, struct hohei* b)
{
  int i,check_a=0,check_b=0;
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(a[i].hp>0){
      check_a=1; break;
    }
  }
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(b[i].hp>0){
      check_b=1; break;
    }
  }
  if(check_a==1 && check_b==1) return 0;
  else if(check_b==0) return 1;
  else if(check_a==0) return 2;
}

void AI(struct hohei* own, struct hohei* enemy, int team)
{
  int num, check_s, check_m, move, x, y, num_enemy, check_a, i, attack, check=0, abs_x, abs_y, xory;
  struct around e[4];
  srand((unsigned)time(NULL));
  while(check!=1){
    check_s=0; check_m=0; check_a=0;
    while(check_s!=1){
      num = rand()%(HOHEI_X*HOHEI_Y);
      check_s = select_AI(own, num);
    }

//printf("%d selected\n", num);

    while(check_m!=1){
      move = rand()%10;
      if(move!=0) move = 1;
      xory = rand()%2;
      if(xory==0){
        x = own[num].x+((rand()%7)-3);
        abs_x = abs(own[num].x-x);
        y = own[num].y+((rand()%(7-abs_x*2))-(3-abs_x));
      }else{
        y = own[num].y+((rand()%7)-3);
        abs_y = abs(own[num].y-y);
        x = own[num].x+((rand()%(7-abs_y*2))-(3-abs_y));
      }
      check_m = move_AI(&own[num], move, x, y);
    }

//if(move==0) printf("did not move\n");
//else        printf("moved to (%d, %d)\n", x, y);

    num_enemy = around_AI(e, own[num], team);
    if(num_enemy!=0){
      while(check_a==0){
        attack = 1;
        i = rand()%4;
        check_a = attack_AI(enemy, team, e, num_enemy, i, attack);
      }
    }
    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      check=1;
      if(own[i].hp!=0 && own[i].moved!=1){
        check=0; break;
      }
    }
  }
}

void AI2(struct hohei* own, struct hohei* enemy, int team)
{
  int num, check_s, check_m, move, x, y, num_enemy, check_a, att_enemy, attack, check=0, abs_x, abs_y, xory, i, j, enemy_team;
  struct around e[4];
  srand((unsigned)time(NULL));

  if(team==1) enemy_team = 2;
  else        enemy_team = 1;

  while(check!=1){
    check_s=0; check_m=0; check_a=0;
    while(check_s!=1){
      num = rand()%(HOHEI_X*HOHEI_Y);
      check_s = select_AI(own, num);
    }

//printf("%d selected\n", num);

    while(check_m!=1){
      move = rand()%10;
      if(move!=0) move=1;
      xory = rand()%2;
      if(xory==0){
        x = own[num].x+((rand()%7)-3);
        abs_x = abs(own[num].x-x);
        y = own[num].y+((rand()%(7-abs_x*2))-(3-abs_x));
      }else{
        y = own[num].y+((rand()%7)-3);
        abs_y = abs(own[num].y-y);
        x = own[num].x+((rand()%(7-abs_y*2))-(3-abs_y));
      }
      check_m = move_AI(&own[num], move, x, y);
    }

//if(move==0) printf("did not move\n");
//else        printf("moved to (%d, %d)\n", x, y);

    num_enemy = around_AI(e, own[num], team);
    if(num_enemy!=0){
      while(check_a==0){
        attack = 1;  att_enemy=-1;
        for(i=0;i<4;i++){
          for(j=0;j<HOHEI_X*HOHEI_Y;j++){
            if(e[i].team==enemy_team && e[i].x==enemy[j].x && e[i].y==enemy[j].y && enemy[j].hp==1){
              att_enemy = i; break;
            }
          }
          if(att_enemy!=-1) break;
        }
        if(att_enemy==-1) att_enemy = rand()%4;
        check_a = attack_AI(enemy, team, e, num_enemy, att_enemy, attack);
      }
    }
    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      check=1;
      if(own[i].hp!=0 && own[i].moved!=1){
        check=0; break;
      }
    }
  }
}

void AI3(struct hohei* own, struct hohei* enemy, int team)
{
  int num, check_s, check_m, move, x, y, num_enemy, check_a, att_enemy, attack, check=0, abs_dx, abs_dy, i, j, enemy_team, xory, area_num=16, dens[area_num], min_area, cent_area[2], dx, dy, prex, prey, hc=0;
  struct around e[4];
  char heis_output[3*3*36+4];

  srand((unsigned)time(NULL));

  if(team==1) enemy_team = 2;
  else        enemy_team = 1;

  while(check!=1){
    check_s=0; check_m=0; check_a=0;
    while(check_s!=1){
      num = rand()%(HOHEI_X*HOHEI_Y);
      check_s = select_AI(own, num);
    }

//printf("%d selected\n", num);

    prex = own[num].x; prey = own[num].y;
    while(check_m!=1){
      move = rand()%20;
      if(move!=0) move=1;
      xory = rand()%2;
      for(i=0;i<area_num;i++)
        dens[i] = density(i%4*5, i/4*5, 5, enemy_team);
      min_area = min_dens(dens, area_num);
      cent_area[0] = min_area%4*5+5/2;
      cent_area[1] = min_area/4*5+5/2;
      if(xory==0){
        dx = (rand()%7)-3;
        if((cent_area[0]-own[num].x>0 && dx<0) || (cent_area[0]-own[num].x<0 && dx>0)) dx = -dx;
        x = own[num].x+dx;
        abs_dx = abs(dx);
        dy = (rand()%(7-abs_dx*2))-(3-abs_dx);
        if((cent_area[1]-own[num].y>0 && dy<0) || (cent_area[1]-own[num].y<0 && dy>0)) dy = -dy;
        y = own[num].y+dy;
      }else{
        dy = (rand()%7)-3;
        if((cent_area[1]-own[num].y>0 && dy<0) || (cent_area[1]-own[num].y<0 && dy>0)) dy = -dy;
        y = own[num].y+dy;
        abs_dy = abs(dy);
        dx = (rand()%(7-abs_dy*2))-(3-abs_dy);
        if((cent_area[0]-own[num].x>0 && dx<0) || (cent_area[0]-own[num].x<0 && dx>0)) dx = -dx;
        x = own[num].x+dx;
      }
      check_m = move_AI(&own[num], move, x, y);
    }

//if(move==0) printf("did not move\n");
//else        printf("moved to (%d, %d)\n", x, y);

    num_enemy = around_AI(e, own[num], team);
    if(num_enemy!=0){
      while(check_a==0){
        attack = 1;  att_enemy=-1;
        for(i=0;i<4;i++){
          for(j=0;j<HOHEI_X*HOHEI_Y;j++){
            if(e[i].team==enemy_team && e[i].x==enemy[j].x && e[i].y==enemy[j].y && enemy[j].hp==1){
              att_enemy = i; break;
            }
          }
          if(att_enemy!=-1) break;
        }
        if(att_enemy==-1) att_enemy = rand()%4;
        check_a = attack_AI(enemy, team, e, num_enemy, att_enemy, attack);
      }
    }

    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      check=1;
      if(own[i].hp!=0 && own[i].moved!=1){
        check=0; 
        break;
      }
    }

    //heis型出力
    for(i=hc;i<hc+3;i++){
      if(i==hc)
        heis_output[i] = (prey*20+prex)/100 + 48;
      else if(i==hc+1)
        heis_output[i] = ((prey*20+prex)%100)/10 + 48;
      else
        heis_output[i] = (prey*20+prex)%10 + 48;
    }
    hc += 3;
    for(i=hc;i<hc+3;i++){
      if(i==hc)
        heis_output[i] = (own[num].y*20+own[num].x)/100 + 48;
      else if(i==hc+1)
        heis_output[i] = ((own[num].y*20+own[num].x)%100)/10 + 48;
      else
        heis_output[i] = (own[num].y*20+own[num].x)%10 + 48;
    }
    hc += 3;
    if(num_enemy==0){
      for(i=hc;i<hc+3;i++){
        if(i==hc)
          heis_output[i] = (own[num].y*20+own[num].x)/100 + 48;
        else if(i==hc+1)
          heis_output[i] = ((own[num].y*20+own[num].x)%100)/10 + 48;
        else
          heis_output[i] = (own[num].y*20+own[num].x)%10 + 48;
      }
      hc += 3;
    }else{
      for(i=hc;i<hc+3;i++){
        if(i==hc)
          heis_output[i] = (e[check_a-num_enemy].y*20+e[check_a-num_enemy].x)/100 + 48;
        else if(i==hc+1)
          heis_output[i] = ((e[check_a-num_enemy].y*20+e[check_a-num_enemy].x)%100)/10 + 48;
        else
          heis_output[i] = (e[check_a-num_enemy].y*20+e[check_a-num_enemy].x)%10 + 48;
      }
      hc += 3;
    }
    //printf("%03d%03d", prey*20+prex, own[num].y*20+own[num].x);
    //if(num_enemy==0) printf("%03d", own[num].y*20+own[num].x);
    //else             printf("%03d", e[check_a-num_enemy].y*20+e[check_a-num_enemy].x);
  }
  heis_output[hc] = '9'; heis_output[hc+1] = '9'; heis_output[hc+2] = '9'; heis_output[hc+3] = '\0';
  printf("%s\n", heis_output);
  //printf("999\n");
}

void AI4(struct hohei* own, struct hohei* enemy, int team)
{
  int num, check_s, check_m, move, x, y, num_enemy, check_a, att_enemy, attack, check=0, abs_dx, abs_dy, i, j, enemy_team, xory, area_num=16, dens[area_num], min_area, cent_area[2], dx, dy;
  struct around e[4];
  srand((unsigned)time(NULL));

  if(team==1) enemy_team = 2;
  else        enemy_team = 1;

  while(check!=1){
    check_s=0; check_m=0; check_a=0;
    while(check_s!=1){
      num = rand()%(HOHEI_X*HOHEI_Y);
      check_s = select_AI(own, num);
    }

//printf("%d selected\n", num);

    num_enemy = around_AI(e, own[num], team);

    while(check_m!=1){
      move = rand()%10;
      if(move!=0) move=1;
      if(num_enemy!=0) move=0;
      xory = rand()%2;
      for(i=0;i<area_num;i++)
        dens[i] = density(i%4*5, i/4*5, 5, enemy_team);
      min_area = min_dens(dens, area_num);
      cent_area[0] = min_area%4*5+5/2;
      cent_area[1] = min_area/4*5+5/2;
      if(xory==0){
        dx = (rand()%7)-3;
        if((cent_area[0]-own[num].x>0 && dx<0) || (cent_area[0]-own[num].x<0 && dx>0)) dx = -dx;
        x = own[num].x+dx;
        abs_dx = abs(dx);
        dy = (rand()%(7-abs_dx*2))-(3-abs_dx);
        if((cent_area[1]-own[num].y>0 && dy<0) || (cent_area[1]-own[num].y<0 && dy>0)) dy = -dy;
        y = own[num].y+dy;
      }else{
        dy = (rand()%7)-3;
        if((cent_area[1]-own[num].y>0 && dy<0) || (cent_area[1]-own[num].y<0 && dy>0)) dy = -dy;
        y = own[num].y+dy;
        abs_dy = abs(dy);
        dx = (rand()%(7-abs_dy*2))-(3-abs_dy);
        if((cent_area[0]-own[num].x>0 && dx<0) || (cent_area[0]-own[num].x<0 && dx>0)) dx = -dx;
        x = own[num].x+dx;
      }
      check_m = move_AI(&own[num], move, x, y);
    }

//if(move==0) printf("did not move\n");
//else        printf("moved to (%d, %d)\n", x, y);

    if(num_enemy!=0){
      while(check_a==0){
        attack = 1;  att_enemy=-1;
        for(i=0;i<4;i++){
          for(j=0;j<HOHEI_X*HOHEI_Y;j++){
            if(e[i].team==enemy_team && e[i].x==enemy[j].x && e[i].y==enemy[j].y && enemy[j].hp==1){
              att_enemy = i; break;
            }
          }
          if(att_enemy!=-1) break;
        }
        if(att_enemy==-1) att_enemy = rand()%4;
        check_a = attack_AI(enemy, team, e, num_enemy, att_enemy, attack);
      }
    }
    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      check=1;
      if(own[i].hp!=0 && own[i].moved!=1){
        check=0; break;
      }
    }
  }
}

void AI5(struct hohei* own, struct hohei* enemy, int team)
{
  int num, check_s, check_m, move, x, y, num_enemy, check_a, att_enemy, attack, check=0, abs_dx, abs_dy, i, j, enemy_team, xory, area_num=16, dens[area_num], min_area, cent_area[2], dx, dy, prex, prey;
  struct around e[4];

  srand((unsigned)time(NULL));

  if(team==1) enemy_team = 2;
  else        enemy_team = 1;

  while(check!=1){
    check_s=0; check_m=0; check_a=0;
    while(check_s!=1){
      num = rand()%(HOHEI_X*HOHEI_Y);
      check_s = select_AI(own, num);
    }

//printf("%d selected\n", num);

    prex = own[num].x; prey = own[num].y;
    while(check_m!=1){
      move = rand()%20;
      if(move!=0) move=1;
      xory = rand()%2;
      for(i=0;i<area_num;i++)
        dens[i] = density(i%4*5, i/4*5, 5, enemy_team);
      min_area = min_dens(dens, area_num);
      cent_area[0] = min_area%4*5+5/2;
      cent_area[1] = min_area/4*5+5/2;
      if(xory==0){
        dx = (rand()%7)-3;
        if((cent_area[0]-own[num].x>0 && dx<0) || (cent_area[0]-own[num].x<0 && dx>0)) dx = -dx;
        x = own[num].x+dx;
        abs_dx = abs(dx);
        dy = (rand()%(7-abs_dx*2))-(3-abs_dx);
        if((cent_area[1]-own[num].y>0 && dy<0) || (cent_area[1]-own[num].y<0 && dy>0)) dy = -dy;
        y = own[num].y+dy;
      }else{
        dy = (rand()%7)-3;
        if((cent_area[1]-own[num].y>0 && dy<0) || (cent_area[1]-own[num].y<0 && dy>0)) dy = -dy;
        y = own[num].y+dy;
        abs_dy = abs(dy);
        dx = (rand()%(7-abs_dy*2))-(3-abs_dy);
        if((cent_area[0]-own[num].x>0 && dx<0) || (cent_area[0]-own[num].x<0 && dx>0)) dx = -dx;
        x = own[num].x+dx;
      }
      check_m = move_AI(&own[num], move, x, y);
    }

//if(move==0) printf("did not move\n");
//else        printf("moved to (%d, %d)\n", x, y);

    num_enemy = around_AI(e, own[num], team);
    if(num_enemy!=0){
      while(check_a==0){
        attack = 1;  att_enemy=-1;
        for(i=0;i<4;i++){
          for(j=0;j<HOHEI_X*HOHEI_Y;j++){
            if(e[i].team==enemy_team && e[i].x==enemy[j].x && e[i].y==enemy[j].y && enemy[j].hp==1){
              att_enemy = i; break;
            }
          }
          if(att_enemy!=-1) break;
        }
        if(att_enemy==-1) att_enemy = rand()%4;
        check_a = attack_AI(enemy, team, e, num_enemy, att_enemy, attack);
      }
    }

    for(i=0;i<HOHEI_X*HOHEI_Y;i++){
      check=1;
      if(own[i].hp!=0 && own[i].moved!=1){
        check=0; 
        break;
      }
    }

    //heis型出力
    //printf("%03d%03d", prey*20+prex, own[num].y*20+own[num].x);
    //if(num_enemy==0) printf("%03d", own[num].y*20+own[num].x);
    //else             printf("%03d", e[check_a-num_enemy].y*20+e[check_a-num_enemy].x);
  }
  //printf("999\n");
}

int can_move(int h_x, int h_y, int x, int y, int enemy)
{
  int check;
  if(x==0 && y==0) return 1;
  else{
    if(stage[h_x][h_y]==enemy) return 0;
    else{
      if(x>0)      check=can_move(h_x+1, h_y, x-1, y, enemy);
      else if(x<0) check=can_move(h_x-1, h_y, x+1, y, enemy);
      if(check==1) return check;
      else{
        if(y>0)      check=can_move(h_x, h_y+1, x, y-1, enemy);
        else if(y<0) check=can_move(h_x, h_y-1, x, y+1, enemy);
      }
      return check;
    }
  }
}

int density(int x, int y, int side_length, int enemy)
{
  int i, j, dens=0;
  for(i=x;i<x+side_length;i++){
    for(j=y;j<y+side_length;j++){
      if(stage[i][j]==enemy) dens++;
    }
  }
  return dens;
}

int min_dens(int *dens, int area_num)
{
  int i, min=HOHEI_X*HOHEI_Y, min_area;
  for(i=0;i<area_num;i++){
    if(dens[i]!=0 && dens[i]<min){
      min=dens[i]; min_area=i;
    }
  }
  return min_area;
}

void heis_input(struct hohei *own, struct hohei *enemy, int team)
{
  char input[3*3*HOHEI_X*HOHEI_Y+4];
  int i, pos, x, y, hohei, moved, atted;
  scanf("%s", input);

  for(i=0;i<3*3*HOHEI_X*HOHEI_Y+4;i+=9){
    if(input[i]=='9' && input[i+1]=='9' && input[i+2]=='9') break;
    pos = (input[i]-'0')*100+(input[i+1]-'0')*10+(input[i+2]-'0');
    x = pos%20;
    y = pos/20;
    hohei = select_h(own, x, y);
    if(hohei<0) continue;
    pos = (input[i+3]-'0')*100+(input[i+4]-'0')*10+(input[i+5]-'0');
    x = pos%20;
    y = pos/20;
    moved = move_h(&own[hohei], x, y);
    if(moved==0){
      own[hohei].moved=1; continue;
    }
    pos = (input[i+6]-'0')*100+(input[i+7]-'0')*10+(input[i+8]-'0');
    x = pos%20;
    y = pos/20;
    atted = attack_h(own[hohei], enemy, team, x, y);
  }
}

int select_h(struct hohei* hoheis, int x, int y)
{
  int i;
  for(i=0;i<HOHEI_X*HOHEI_Y;i++){
    if(hoheis[i].x==x && hoheis[i].y==y && hoheis[i].moved!=1)
      return i;
  }
  return -1;
}

int move_h(struct hohei* hohei, int x, int y)
{
  int team=stage[hohei->x][hohei->y], enemy;
  if(team==1) enemy=2;
  else        enemy=1;
  if(hohei->x==x && hohei->y==y)
    return 1;
  if(x>=STAGE_X || y>=STAGE_Y || x<0 || y<0 || stage[x][y]!=0 || abs(x-hohei->x)+abs(y-hohei->y)>3)
    return 0;
  if(can_move(hohei->x, hohei->y, x-hohei->x, y-hohei->y, enemy)!=1)
    return 0;
  stage[hohei->x][hohei->y]=0;
  stage[x][y]=team;
  hohei->x=x;
  hohei->y=y;
  hohei->moved=1;
  return 1;
}

int attack_h(struct hohei own, struct hohei* enemy, int team, int x, int y)
{
  int i, team_enemy;

  if(team==1) team_enemy=2;
  else        team_enemy=1;

  if(!((abs(own.x-x)==1 && own.y==y) || (abs(own.y-y)==1 && own.x==x)))
    return 0;
  else{
    if(x<0 || x>STAGE_X || y<0 || y>STAGE_Y || stage[x][y]!=team_enemy)
      return 0;
    else{
      for(i=0;i<HOHEI_X*HOHEI_Y;i++){
        if(enemy[i].x==x && enemy[i].y==y && enemy[i].hp>0){
          enemy[i].hp--;
          if(enemy[i].hp<=0)
            stage[x][y]=0;
          return 1;
        }
      }
    }
  }
}
